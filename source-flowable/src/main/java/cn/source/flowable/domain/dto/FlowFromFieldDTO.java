package cn.source.flowable.domain.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 詹Sir
 * @date 2021/3/31 23:20
 */
@Data
public class FlowFromFieldDTO implements Serializable {

    private Object fields;
}
